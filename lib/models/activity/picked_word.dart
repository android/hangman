import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

class PickedWord {
  const PickedWord({
    required this.word,
    required this.clue,
  });

  final String word;
  final String clue;

  static const PickedWord none = PickedWord(
    word: '',
    clue: '',
  );

  void dump() {
    printlog('$PickedWord:');
    printlog('  word: $word');
    printlog('  clue: $clue');
    printlog('');
  }

  @override
  String toString() {
    return '$PickedWord(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      'word': word,
      'clue': clue,
    };
  }
}
