import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:hangman/config/application_config.dart';

typedef MovingTile = String;
typedef Player = String;
typedef ConflictsCount = List<List<int>>;
typedef AnimatedBoard = List<List<bool>>;
typedef AnimatedBoardSequence = List<AnimatedBoard>;
typedef Word = String;

class Activity {
  Activity({
    // Settings
    required this.activitySettings,

    // State
    this.isRunning = false,
    this.isStarted = false,
    this.isFinished = false,
    this.animationInProgress = false,
    this.gettingSecretWord = false,

    // Base data
    required this.secretWord,
    required this.clue,

    // Game data
    required this.hiddenWord,
    required this.usedLetters,
    this.errorsCount = 0,
    this.isClueDisplayed = false,
  });

  // Settings
  final ActivitySettings activitySettings;

  // State
  bool isRunning;
  bool isStarted;
  bool isFinished;
  bool animationInProgress;
  bool gettingSecretWord;

  // Base data
  String secretWord;
  String clue;

  // Game data
  List<String> hiddenWord = [];
  List<String> usedLetters = [];
  int errorsCount;
  bool isClueDisplayed;

  factory Activity.createNull() {
    return Activity(
      // Settings
      activitySettings: ActivitySettings.createDefault(appConfig: ApplicationConfig.config),
      // Base data
      secretWord: '',
      clue: '',
      // Game data
      hiddenWord: [],
      usedLetters: [],
    );
  }

  factory Activity.createNew({
    ActivitySettings? activitySettings,
  }) {
    final ActivitySettings newActivitySettings = activitySettings ??
        ActivitySettings.createDefault(appConfig: ApplicationConfig.config);

    return Activity(
      // Settings
      activitySettings: newActivitySettings,
      // State
      isRunning: true,
      gettingSecretWord: true,
      // Base data
      secretWord: '',
      clue: '',
      // Game data
      hiddenWord: [],
      usedLetters: [],
    );
  }

  String get hiddenWordAsString => hiddenWord.join();

  bool get canBeResumed => isStarted && !isFinished;
  bool get gameWon => isFinished && (hiddenWordAsString == secretWord);

  void dump() {
    printlog('');
    printlog('## Current game dump:');
    printlog('');
    printlog('$Activity:');
    printlog('  Settings');
    activitySettings.dump();
    printlog('  State');
    printlog('    isRunning: $isRunning');
    printlog('    isStarted: $isStarted');
    printlog('    isFinished: $isFinished');
    printlog('    animationInProgress: $animationInProgress');
    printlog('    gettingSecretWord: $gettingSecretWord');
    printlog('  Base data');
    printlog('    secretWord: $secretWord');
    printlog('    clue: $clue');
    printlog('  Game data');
    printlog('    hiddenWord: $hiddenWord');
    printlog('    usedLetters: $usedLetters');
    printlog('    errorsCount: $errorsCount');
    printlog('    isClueDisplayed: $isClueDisplayed');
    printlog('');
  }

  @override
  String toString() {
    return '$Activity(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      // Settings
      'activitySettings': activitySettings.toJson(),
      // State
      'isRunning': isRunning,
      'isStarted': isStarted,
      'isFinished': isFinished,
      'animationInProgress': animationInProgress,
      'gettingSecretWord': gettingSecretWord,
      // Base data
      'secretWord': secretWord,
      'clue': clue,
      // Game data
      'hiddenWord': hiddenWord,
      'usedLetters': usedLetters,
      'errorsCount': errorsCount,
      'isClueDisplayed': isClueDisplayed,
    };
  }
}
