import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:hangman/data/fetch_data_helper.dart';
import 'package:hangman/models/activity/activity.dart';
import 'package:hangman/models/activity/picked_word.dart';

part 'activity_state.dart';

class ActivityCubit extends HydratedCubit<ActivityState> {
  ActivityCubit()
      : super(ActivityState(
          currentActivity: Activity.createNull(),
        ));

  void updateState(Activity activity) {
    emit(ActivityState(
      currentActivity: activity,
    ));
  }

  void refresh() {
    final Activity activity = Activity(
      // Settings
      activitySettings: state.currentActivity.activitySettings,
      // State
      isRunning: state.currentActivity.isRunning,
      isStarted: state.currentActivity.isStarted,
      isFinished: state.currentActivity.isFinished,
      animationInProgress: state.currentActivity.animationInProgress,
      gettingSecretWord: state.currentActivity.gettingSecretWord,
      // Base data
      secretWord: state.currentActivity.secretWord,
      clue: state.currentActivity.clue,
      // Game data
      hiddenWord: state.currentActivity.hiddenWord,
      usedLetters: state.currentActivity.usedLetters,
      errorsCount: state.currentActivity.errorsCount,
      isClueDisplayed: state.currentActivity.isClueDisplayed,
    );
    // game.dump();

    updateState(activity);
  }

  void startNewActivity(BuildContext context) {
    final ActivitySettingsCubit activitySettingsCubit =
        BlocProvider.of<ActivitySettingsCubit>(context);

    final ActivitySettings activitySettings = activitySettingsCubit.state.settings;

    final Activity newActivity = Activity.createNew(
      // Settings
      activitySettings: activitySettings,
    );

    updateState(newActivity);
    refresh();

    FetchDataHelper().pickRandomWord(activitySettings).then((PickedWord pickedWord) {
      state.currentActivity.gettingSecretWord = false;
      state.currentActivity.secretWord = pickedWord.word;
      state.currentActivity.clue = pickedWord.clue;
      state.currentActivity.hiddenWord =
          List<String>.generate(pickedWord.word.length, (i) => '_');
      state.currentActivity.dump();

      refresh();
    });
  }

  bool canBeResumed() {
    return state.currentActivity.canBeResumed;
  }

  void quitActivity() {
    state.currentActivity.isRunning = false;
    refresh();
  }

  void resumeSavedActivity() {
    state.currentActivity.isRunning = true;
    refresh();
  }

  void deleteSavedActivity() {
    state.currentActivity.isRunning = false;
    state.currentActivity.isFinished = true;
    refresh();
  }

  void submitLetter(String key) {
    if (!state.currentActivity.usedLetters.contains(key)) {
      updateUsedLetters(key);

      if (state.currentActivity.secretWord.contains(key)) {
        for (int index = 0; index < state.currentActivity.secretWord.length; index++) {
          if (key == state.currentActivity.secretWord[index]) {
            updateHiddenWord(index, key);
          }
        }
      } else {
        incrementErrorsCount();
      }
    }

    if ((state.currentActivity.hiddenWordAsString == state.currentActivity.secretWord) ||
        (state.currentActivity.errorsCount == 8)) {
      state.currentActivity.isFinished = true;
    }

    refresh();
  }

  void updateUsedLetters(String key) {
    state.currentActivity.usedLetters.add(key);
    refresh();
  }

  void updateHiddenWord(int index, String letter) {
    state.currentActivity.hiddenWord[index] = letter;
    refresh();
  }

  void incrementErrorsCount() {
    state.currentActivity.errorsCount++;
    refresh();
  }

  void toggleClueDisplay() {
    state.currentActivity.isClueDisplayed = !state.currentActivity.isClueDisplayed;
    refresh();
  }

  @override
  ActivityState? fromJson(Map<String, dynamic> json) {
    final Activity currentActivity = json['currentActivity'] as Activity;

    return ActivityState(
      currentActivity: currentActivity,
    );
  }

  @override
  Map<String, dynamic>? toJson(ActivityState state) {
    return <String, dynamic>{
      'currentActivity': state.currentActivity.toJson(),
    };
  }
}
