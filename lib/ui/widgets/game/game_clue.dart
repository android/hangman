import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:hangman/cubit/activity/activity_cubit.dart';
import 'package:hangman/models/activity/activity.dart';

class GameDisplayClueWidget extends StatelessWidget {
  const GameDisplayClueWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: BlocBuilder<ActivityCubit, ActivityState>(
        builder: (BuildContext context, ActivityState activityState) {
          final Activity currentActivity = activityState.currentActivity;

          return TextButton(
            onPressed: () {
              BlocProvider.of<ActivityCubit>(context).toggleClueDisplay();
            },
            child: FittedBox(
              child: Text(
                currentActivity.isClueDisplayed ? currentActivity.clue : '?',
                style: const TextStyle(
                  fontFamily: 'Tiza',
                  color: Colors.black,
                  fontSize: 24.0,
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
