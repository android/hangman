import 'package:flutter/material.dart';

class GameSomethingWentWrongWidget extends StatelessWidget {
  const GameSomethingWentWrongWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text("Something went wrong..."),
    );
  }
}
