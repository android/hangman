import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:hangman/cubit/activity/activity_cubit.dart';
import 'package:hangman/models/activity/activity.dart';

class GameTopWidget extends StatelessWidget {
  const GameTopWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;
        return Stack(
          children: [
            for (int error = 0; error < 9; error++)
              AnimatedOpacity(
                opacity: currentActivity.errorsCount >= error ? 1.0 : 0.0,
                duration:
                    Duration(milliseconds: currentActivity.errorsCount >= error ? 500 : 0),
                child: Image.asset('assets/skins/default_img${error + 1}.png'),
              )
          ],
        );
      },
    );
  }
}
