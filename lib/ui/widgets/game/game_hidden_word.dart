import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:hangman/cubit/activity/activity_cubit.dart';
import 'package:hangman/models/activity/activity.dart';

class GameHiddenWordWidget extends StatelessWidget {
  const GameHiddenWordWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: BlocBuilder<ActivityCubit, ActivityState>(
        builder: (BuildContext context, ActivityState activityState) {
          final Activity currentActivity = activityState.currentActivity;

          return FittedBox(
            child: Text(
              currentActivity.hiddenWordAsString,
              style: const TextStyle(
                fontFamily: 'Tiza',
                color: Colors.white,
                fontSize: 34.0,
                letterSpacing: 10.0,
              ),
            ),
          );
        },
      ),
    );
  }
}
