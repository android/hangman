import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:hangman/cubit/activity/activity_cubit.dart';
import 'package:hangman/models/activity/activity.dart';
import 'package:hangman/ui/widgets/game/game_virtual_keyboard_key.dart';

class GameVirtualKeyboardWidget extends StatelessWidget {
  const GameVirtualKeyboardWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        final List<List<String>> keys = [
          ['A', 'Z', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P'],
          ['Q', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M'],
          [' ', ' ', 'W', 'X', 'C', 'V', 'B', 'N', ' ', ' '],
        ];

        List<TableRow> tableRows = [];
        for (List<String> row in keys) {
          List<TableCell> tableCells = [];
          for (String key in row) {
            tableCells.add(TableCell(
              child: GameVirtualKeyboardKeyWidget(
                caption: key,
                enabled: !currentActivity.usedLetters.contains(key),
              ),
            ));
          }
          tableRows.add(TableRow(children: tableCells));
        }

        return SizedBox(
          height: 150,
          width: double.maxFinite,
          child: Container(
            margin: const EdgeInsets.symmetric(horizontal: 2),
            padding: const EdgeInsets.all(2),
            child: Table(
              defaultVerticalAlignment: TableCellVerticalAlignment.middle,
              children: tableRows,
            ),
          ),
        );
      },
    );
  }
}
