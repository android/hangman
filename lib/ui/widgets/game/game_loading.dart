import 'package:flutter/material.dart';

class GameLoadingIndicatorWidget extends StatelessWidget {
  const GameLoadingIndicatorWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }
}
