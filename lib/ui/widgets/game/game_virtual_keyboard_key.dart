import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:hangman/config/game_colors.dart';
import 'package:hangman/cubit/activity/activity_cubit.dart';

class GameVirtualKeyboardKeyWidget extends StatelessWidget {
  const GameVirtualKeyboardKeyWidget({
    super.key,
    required this.caption,
    required this.enabled,
  });

  final String caption;
  final bool enabled;

  @override
  Widget build(BuildContext context) {
    final Color keyColor = enabled ? Colors.black : GameColors.keyColor;

    if (caption == ' ') {
      return const SizedBox();
    }

    return Padding(
      padding: const EdgeInsets.all(2),
      child: AspectRatio(
        aspectRatio: 1,
        child: TextButton(
          style: TextButton.styleFrom(
            padding: const EdgeInsets.all(0),
            backgroundColor: GameColors.keyColor,
          ),
          child: Text(
            caption,
            style: TextStyle(
              color: keyColor,
              fontSize: 24.0,
              fontWeight: FontWeight.w800,
            ),
            textAlign: TextAlign.center,
          ),
          onPressed: () {
            if (caption != ' ') {
              BlocProvider.of<ActivityCubit>(context).submitLetter(caption);
            }
          },
        ),
      ),
    );
  }
}
