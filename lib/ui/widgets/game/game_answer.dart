import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:hangman/cubit/activity/activity_cubit.dart';
import 'package:hangman/models/activity/activity.dart';

class GameDisplayAnswerWidget extends StatelessWidget {
  const GameDisplayAnswerWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: BlocBuilder<ActivityCubit, ActivityState>(
        builder: (BuildContext context, ActivityState activityState) {
          final Activity currentActivity = activityState.currentActivity;

          return Padding(
            padding: const EdgeInsets.all(8),
            child: FittedBox(
              child: Text(
                currentActivity.secretWord,
                style: const TextStyle(
                  fontFamily: 'Tiza',
                  fontSize: 36.0,
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
