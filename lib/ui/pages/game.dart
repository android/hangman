import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:hangman/config/game_colors.dart';
import 'package:hangman/cubit/activity/activity_cubit.dart';
import 'package:hangman/models/activity/activity.dart';
import 'package:hangman/ui/widgets/game/game_answer.dart';
import 'package:hangman/ui/widgets/game/game_clue.dart';
import 'package:hangman/ui/widgets/game/game_hidden_word.dart';
import 'package:hangman/ui/widgets/game/game_loading.dart';
import 'package:hangman/ui/widgets/game/game_something_went_wrong.dart';
import 'package:hangman/ui/widgets/game/game_virtual_keyboard.dart';
import 'package:hangman/ui/widgets/game/game_end.dart';
import 'package:hangman/ui/widgets/game/game_top.dart';

class PageGame extends StatelessWidget {
  const PageGame({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        if (currentActivity.gettingSecretWord) {
          return const GameLoadingIndicatorWidget();
        }

        if (currentActivity.secretWord == '') {
          return const GameSomethingWentWrongWidget();
        }

        return Container(
          alignment: AlignmentDirectional.topCenter,
          padding: const EdgeInsets.all(4),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                color: GameColors.boardColor,
                alignment: AlignmentDirectional.topCenter,
                padding: const EdgeInsets.all(12),
                child: const Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: 24),
                    GameTopWidget(),
                    SizedBox(height: 24),
                    GameHiddenWordWidget(),
                    SizedBox(height: 24),
                  ],
                ),
              ),
              currentActivity.clue != ''
                  ? const GameDisplayClueWidget()
                  : const SizedBox.shrink(),
              currentActivity.isFinished && !currentActivity.gameWon
                  ? const GameDisplayAnswerWidget()
                  : const SizedBox.shrink(),
              const Expanded(child: SizedBox.shrink()),
              currentActivity.isFinished
                  ? const GameEndWidget()
                  : const GameVirtualKeyboardWidget(),
            ],
          ),
        );
      },
    );
  }
}
