import 'dart:ui';

class GameColors {
  static const Color boardColor = Color(0xff004D00);
  static const Color keyColor = Color(0xFFD59500);
}
