import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:hangman/cubit/activity/activity_cubit.dart';

import 'package:hangman/ui/pages/game.dart';

import 'package:hangman/ui/parameters/parameter_painter_difficulty_level.dart';

class ApplicationConfig {
  // activity parameter: game mode
  static const String parameterCodeGameMode = 'activity.gameMode';
  static const String gameModeValueOnline = 'online';
  static const String gameModeValueOffline = 'offline';

  // activity parameter: game level
  static const String parameterCodeGameLevel = 'activity.gameLevel';
  static const String gameLevelValueEasy = 'easy';
  static const String gameLevelValueHard = 'hard';

  // activity pages
  static const int activityPageIndexHome = 0;
  static const int activityPageIndexGame = 1;

  static final ApplicationConfigDefinition config = ApplicationConfigDefinition(
    appTitle: 'Hangman',
    activitySettings: [
      // game mode
      ApplicationSettingsParameter(
        code: parameterCodeGameMode,
        values: [
          ApplicationSettingsParameterItemValue(
            value: gameModeValueOnline,
            text: '🌐',
            color: Colors.teal,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: gameModeValueOffline,
            text: '📱',
            color: Colors.purple,
          ),
        ],
        builder: ({required context, required itemValue, required onPressed, required size}) {
          return StyledButton.icon(
            onPressed: onPressed,
            icon: Icon(itemValue.value == gameModeValueOnline
                ? UniconsLine.globe
                : UniconsLine.clipboard),
            color: itemValue.color ?? Colors.grey,
          );
        },
      ),

      // game level
      ApplicationSettingsParameter(
        code: parameterCodeGameLevel,
        values: [
          ApplicationSettingsParameterItemValue(
            value: gameLevelValueEasy,
            color: Colors.green,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: gameLevelValueHard,
            color: Colors.orange,
          ),
        ],
        customPainter: (context, value) => ParameterPainterDifficultyLevel(
          context: context,
          value: value,
        ),
      ),
    ],
    startNewActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).startNewActivity(context);
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    quitCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).quitActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexHome);
    },
    deleteCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).deleteSavedActivity();
    },
    resumeActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).resumeSavedActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    navigation: ApplicationNavigation(
      screenActivity: ScreenItem(
        code: 'screen_activity',
        icon: Icon(UniconsLine.home),
        screen: ({required ApplicationConfigDefinition appConfig}) =>
            ScreenActivity(appConfig: appConfig),
      ),
      screenSettings: ScreenItem(
        code: 'screen_settings',
        icon: Icon(UniconsLine.setting),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenSettings(),
      ),
      screenAbout: ScreenItem(
        code: 'screen_about',
        icon: Icon(UniconsLine.info_circle),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenAbout(),
      ),
      activityPages: {
        activityPageIndexHome: ActivityPageItem(
          code: 'page_home',
          icon: Icon(UniconsLine.home),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageParameters(
                  appConfig: appConfig,
                  canBeResumed: activityState.currentActivity.canBeResumed,
                );
              },
            );
          },
        ),
        activityPageIndexGame: ActivityPageItem(
          code: 'page_game',
          icon: Icon(UniconsLine.star),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageGame();
              },
            );
          },
        ),
      },
    ),
  );
}
