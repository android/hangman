const List<dynamic> dictionaryFrenchEasy = [
  {
    "category": "ANIMAUX",
    "clue": "Animal",
    "words": [
      "ANACONDA",
      "AUTRUCHE",
      "BARRACUDA",
      "BOUQUETIN",
      "COCCINELLE",
      "CROCODILE",
      "DROMADAIRE",
      "ELEPHANT",
      "ESCARGOT",
      "FOURMILIER",
      "GRENOUILLE",
      "HIPPOCAMPE",
      "HIPPOPOTAME",
      "KANGOUROU",
      "LIBELLULE",
      "PERROQUET",
      "PIPISTRELLE",
      "RHINOCEROS",
      "SAUTERELLE",
      "TARENTULE"
    ]
  },
  {
    "category": "FRUITS",
    "clue": "Fruit",
    "words": [
      "AUBERGINE",
      "BETTERAVE",
      "CITROUILLE",
      "CONCOMBRE",
      "FRAMBOISE",
      "GROSEILLE",
      "MANDARINE",
      "MIRABELLE",
      "MYRTILLE",
      "PAMPLEMOUSSE"
    ]
  },
  {
    "category": "METIERS",
    "clue": "Métier",
    "words": [
      "AGRICULTEUR",
      "ARCHEOLOGUE",
      "ARCHITECTE",
      "ASTRONAUTE",
      "BIJOUTIER",
      "BIOLOGISTE",
      "CHARCUTIER",
      "CHARPENTIER",
      "CUISINIER",
      "ELECTRICIEN",
      "HORTICULTEUR",
      "INFIRMIER",
      "MECANICIEN",
      "MENUISIER",
      "METEOROLOGUE",
      "PHOTOGRAPHE",
      "PROFESSEUR",
      "STANDARDISTE",
      "VETERINAIRE",
      "VOLCANOLOGUE"
    ]
  },
  {
    "category": "GEOGRAPHIE",
    "clue": "Géographie",
    "words": [
      "ALLEMAGNE",
      "ANTARCTIQUE",
      "ARGENTINE",
      "ATLANTIQUE",
      "AUSTRALIE",
      "EMBOUCHURE",
      "HEMISPHERE",
      "HYDROGRAPHIE",
      "KILIMANDJARO",
      "LUXEMBOURG",
      "MADAGASCAR",
      "MEDITERRANEE",
      "MISSISSIPPI",
      "NORMANDIE",
      "PACIFIQUE",
      "PLANISPHERE",
      "STRASBOURG",
      "SUPERFICIE",
      "VENEZUELA",
      "WASHINGTON"
    ]
  },
  {
    "category": "COULEURS",
    "clue": "Couleur",
    "words": [
      "ROUGE",
      "BLEU",
      "VERT",
      "JAUNE",
      "VIOLET",
      "ORANGE",
      "MARRON",
      "NOIR",
      "BLANC",
      "TURQUOISE",
      "BEIGE",
      "ROSE"
    ]
  },
  {
    "category": "FLEURS",
    "clue": "Fleur",
    "words": [
      "ROSE",
      "PIVOINE",
      "TULIPE",
      "JONQUILLE",
      "CACTUS",
      "PETUNIA",
      "COQUELICOT",
      "JACINTHE"
    ]
  },
  {
    "category": "SPORTS",
    "clue": "Sport ou jeu",
    "words": [
      "GYMNASTIQUE",
      "FOOTBALL",
      "HANDBALL",
      "COURSE",
      "CYCLISME",
      "RANDONNEE",
      "NATATION",
      "KARATE",
      "ESCRIME",
      "EQUITATION"
    ]
  },
  {
    "category": "ALIMENTS",
    "clue": "Aliment ou plat",
    "words": [
      "FROMAGE",
      "PIZZA",
      "SAUCISSON",
      "JAMBON",
      "SALAMI",
      "PAELLA",
      "PATES",
      "SALADE",
      "SOUPE",
      "CHOCOLAT",
      "OEUF",
      "CREME",
      "LAIT",
      "CORNICHON",
      "FLAN",
      "TARTE",
      "PUREE",
      "SAUMON",
      "SANDWICH"
    ]
  },
  {
    "category": "VEHICULE",
    "clue": "Véhicule ou moyen de transport",
    "words": [
      "VOITURE",
      "MOTO",
      "VELO",
      "TRAIN",
      "BATEAU",
      "AVION",
      "HELICOPTERE",
      "AUTOBUS",
      "CAR",
      "TRAINEAU",
      "FUSEE",
      "VOILIER",
      "PAQUEBOT",
      "METRO",
      "SOUS-MARIN",
      "CAMION",
      "TRACTEUR",
      "KAYAK"
    ]
  }
];
