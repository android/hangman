import 'dart:async';
import 'dart:convert';
import 'dart:math' show Random;

import 'package:diacritic/diacritic.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:html/parser.dart';
import 'package:html/dom.dart';
import 'package:http/http.dart' as http;

import 'package:hangman/config/application_config.dart';
import 'package:hangman/data/dictionary_french_easy.dart';
import 'package:hangman/data/dictionary_french_hard.dart';
import 'package:hangman/models/activity/picked_word.dart';

class FetchDataHelper {
  FetchDataHelper();

  Future<PickedWord> pickRandomWord(ActivitySettings activitySettings) async {
    switch (activitySettings.get(ApplicationConfig.parameterCodeGameMode)) {
      case ApplicationConfig.gameModeValueOffline:
        return Future(() => pickRandomOfflineWord(activitySettings));
      case ApplicationConfig.gameModeValueOnline:
        return await pickRandomOnlineWord(activitySettings);
    }

    return PickedWord.none;
  }

  PickedWord pickRandomOfflineWord(ActivitySettings activitySettings) {
    switch (activitySettings.get(ApplicationConfig.parameterCodeGameLevel)) {
      case ApplicationConfig.gameLevelValueEasy:
        return wordFromLocalEasyDictionary();
      case ApplicationConfig.gameLevelValueHard:
        return wordFromLocalFullDictionary();
    }

    return PickedWord.none;
  }

  Future<PickedWord> pickRandomOnlineWord(ActivitySettings activitySettings) {
    const baseUrl = 'https://www.palabrasaleatorias.com/mots-aleatoires.php';
    switch (activitySettings.get(ApplicationConfig.parameterCodeGameLevel)) {
      case ApplicationConfig.gameLevelValueEasy:
        return wordFromWeb('$baseUrl?fs=1&fs2=0&Submit=Nouveau+mot');
      case ApplicationConfig.gameLevelValueHard:
        return wordFromWeb('$baseUrl?fs=1&fs2=1&Submit=Nouveau+mot');
    }
    return Future(() => PickedWord.none);
  }

  Future<PickedWord> wordFromWeb(String url) async {
    try {
      final response = await http.Client().get(Uri.parse(url));
      if (response.statusCode == 200) {
        final html = parse(utf8.decode(response.bodyBytes));
        Element element = html.getElementsByTagName('div').last;
        String word = clean(element.text);
        if (word == '') {
          throw Exception();
        }
        return PickedWord(
          word: word,
          clue: '',
        );
      } else {
        throw Exception();
      }
    } catch (e) {
      return PickedWord.none;
    }
  }

  PickedWord wordFromLocalFullDictionary() {
    final String rawWord = dictionaryFrench[Random().nextInt(dictionaryFrench.length)];

    return PickedWord(
      word: clean(rawWord),
      clue: '',
    );
  }

  PickedWord wordFromLocalEasyDictionary() {
    final Map<String, dynamic> randomCategory =
        dictionaryFrenchEasy[Random().nextInt(dictionaryFrenchEasy.length)];

    final clue = randomCategory['clue'];
    final words = randomCategory['words'] as List<String>;

    final word = words[Random().nextInt(words.length)];

    return PickedWord(
      word: clean(word),
      clue: clue,
    );
  }

  String clean(String wordCandidate) {
    String word = wordCandidate.trim();

    if (word.contains(' ')) {
      return '';
    }

    word = removeDiacritics(word);
    word = word.toUpperCase();
    word = word.replaceAll(RegExp('[0-9]'), '');
    word = word.replaceAll(RegExp('[^A-Z]'), '');

    if (word.length < 3 || word.length > 12) {
      return '';
    }

    return word;
  }
}
