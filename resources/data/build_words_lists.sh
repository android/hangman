#!/usr/bin/env bash

command -v jq >/dev/null 2>&1 || {
  echo >&2 "I require jq (json parser) but it's not installed. Aborting."
  exit 1
}

CURRENT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
BASE_DIR="$(dirname "$(dirname "${CURRENT_DIR}")")"

# JSON date file for frean "easy" words
DICTIONARY_JSON_FILE="${CURRENT_DIR}/dictionary-fr-easy.json"
if [ ! -f "${DICTIONARY_JSON_FILE}" ]; then
  echo "File ${DICTIONARY_JSON_FILE} not found. Abort."
  exit 1
fi
# DART source code file to inject data into
GAME_DATA_DART_FILE="${BASE_DIR}/lib/data/dictionary_french_easy.dart"
if [ ! -f "${GAME_DATA_DART_FILE}" ]; then
  echo "File ${GAME_DATA_DART_FILE} not found. Abort."
  exit 1
fi

echo "Injecting json file ${DICTIONARY_JSON_FILE} in ${GAME_DATA_DART_FILE}..."
echo "const List<dynamic> dictionaryFrenchEasy = $(cat "${DICTIONARY_JSON_FILE}");" >"${GAME_DATA_DART_FILE}"

echo "Formatting dart source code file..."
dart format "${GAME_DATA_DART_FILE}"

echo "done."
